# **Create Clone Project**

1. Create a clone from this url 'git clone https://gitlab.com/Ihjazfathima/ef-with-sql-server.git '
2. You will get a folder named EF with SQL server.
3. Open EF with SQL server folder.

# **Working Process**

1. Run the application in visual studio 2019 and select the application url http://localhost:5000/users/all
2. Automatically the link will open in chrome.
3. You can see all the user details like Id , Name , Age , Email , phone_no .
