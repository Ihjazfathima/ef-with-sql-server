﻿using EF_with_SqlServer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_with_SqlServer.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class usersController : ControllerBase
    {
        private readonly userDBContext _context;
        public usersController(userDBContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("all")]
        public async Task<ActionResult<IEnumerable<user>>> Getuser()
        {
            return await _context.users.ToListAsync();
        }

    }
}
