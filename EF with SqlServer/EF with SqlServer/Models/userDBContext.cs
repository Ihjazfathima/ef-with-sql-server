﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_with_SqlServer.Models
{
    public class userDBContext : DbContext
    {
        public userDBContext(DbContextOptions<userDBContext> options) : base(options) { }
        public DbSet<user> users { get; set; }
    }
}
